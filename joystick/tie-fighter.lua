-- Physical devices to use
devices =
    {
        d0 = --Thrustmaster Warthog Joystick
        {
	    vendorid = 0x044f,
	    productid = 0x0402,
	 },

      d1 = --Thrustmaster Warthog Throttle
	 {
	    vendorid = 0x044f,
	    productid = 0x0404,
	 }
 }

--Virtual devices to create, current limit is maximum 53 (0 to 52) buttons and 19 (0 to 18) axes. Note that not every button or axis is fully tested to work.
--Creating more than one virtual devices is possible, making room for more buttons and axes.
v_devices = 
    {
    v0 = 
    {
        buttons = 16,
        axes = 4
    }
}

-- Left/right axis
function d0_a0_event(value)
    send_axis_event(0, 0, value)
end

-- Up/down axis
function d0_a1_event(value)
    send_axis_event(0, 1, value)
end

-- Fire lasers
function d0_b0_event(value)
    print("Firing weapon ", value)
    send_button_event(0, 0, value)
end

-- d0_b4: Joystick, index finger button on right of stick
-- Roll/Target current ship in sights
function d0_b4_event(value)
    print("Rolling: ", value)
    send_button_event(0, 1, value)
end

-- Throttle
MAX_VALUE = 65534   -- 100%
FULL_SPEED = 3
TWO_THIRDS = 2
ONE_THIRD = 1
ALL_STOP = 0

current_speed = FULL_SPEED

function d1_a3_event(value)
    value = (value * -1) + 32767
    position = math.floor((value / MAX_VALUE) * 3)
    if (position == FULL_SPEED and current_speed ~= FULL_SPEED) then
        send_keyboard_event(KEY_BACKSPACE, 1)
        send_keyboard_event(KEY_BACKSPACE, 0)
        current_speed = FULL_SPEED
        print("Going to full power")
    elseif (position == TWO_THIRDS and current_speed ~= TWO_THIRDS) then
        send_keyboard_event(KEY_RIGHTBRACE, 1)
        send_keyboard_event(KEY_RIGHTBRACE, 0)
        current_speed = TWO_THIRDS
        print("Going to 2/3")
    elseif (position == ONE_THIRD and current_speed ~= ONE_THIRD) then
        send_keyboard_event(KEY_LEFTBRACE, 1)
        send_keyboard_event(KEY_LEFTBRACE, 0)
        current_speed = ONE_THIRD
        print("Going to 1/3")
    elseif (position == ALL_STOP and current_speed ~= ALL_STOP) then
        send_keyboard_event(KEY_BACKSLASH, 1)
        send_keyboard_event(KEY_BACKSLASH, 0)
        current_speed = ALL_STOP
        print("Answering all stop")
    end
end

-- d1_b1: Throttle, thumb hat press
function d1_b1_event(value)
    if value == 1 then
        send_keyboard_event(KEY_F8, 1)
        send_keyboard_event(KEY_F8, 0)
    end
end
-- d1_b2: Throttle, thumb hat up
function d1_b2_event(value)
    if value == 1 then
        send_keyboard_event(KEY_F10, 1)
        send_keyboard_event(KEY_F10, 0)
    end
end
-- d1_b3: Throttle, thumb hat forward
function d1_b3_event(value)
    if value == 1 then
        send_keyboard_event(KEY_F9, 1)
        send_keyboard_event(KEY_F9, 1)
        send_keyboard_event(KEY_F9, 0)
        print("Setting lasers")
    end
end
-- d1_b4: Throttle, thumb hat down
function d1_b4_event(value)
    if value == 1 then
        send_keyboard_event(KEY_COMMA, 1)
        send_keyboard_event(KEY_COMMA, 0)
    end
end
-- d1_b5: Throttle, thumb hat back
function d1_b3_event(value)
    if value == 1 then
        send_keyboard_event(KEY_SEMICOLON, 1)
        send_keyboard_event(KEY_SEMICOLON, 0)
        print("Setting lasers")
    end
end
