-- Physical devices to use
devices =
    {
        d0 = --Thrustmaster Warthog Joystick
        {
	    vendorid = 0x044f,
	    productid = 0x0402,
	 },

      d1 = --Thrustmaster Warthog Throttle
	 {
	    vendorid = 0x044f,
	    productid = 0x0404,
	 }
 }

--Virtual devices to create, current limit is maximum 53 (0 to 52) buttons and 19 (0 to 18) axes. Note that not every button or axis is fully tested to work.
--Creating more than one virtual devices is possible, making room for more buttons and axes.
v_devices = 
    {
    v0 = 
    {
        buttons = 2,
        axes = 4
    }
}

-- d0_a0: Joystick, left/right axis
-- d0_a1: Joystick, up/down axis
-- d0_a2: Joystick, grey hat left/right
-- d0_a3: Joystick, grey hat up/down
-- d0:b0: Joystick, main trigger
-- d0_b1: Joystick, red button top left
-- d0_b2: Joystick, pinky button
-- d0_b3: Joystick, pinky lever
-- d0_b4: Joystick, index finger button on right of stick
-- d0_b5: Joystick, main trigger fully down
-- d0_b6: Joystick, round black hat up
-- d0_b7: Joystick, round black hat right
-- d0_b8: Joystick, round black hat down
-- d0_b9: Joystick, round black hat left
-- d0_b10: Joystick, flat black hat up
-- d0_b11: Joystick, flat black hat right
-- d0_b12: Joystick, flat black hat down
-- d0_b13: Joystick, flat black hat left
-- d0_b14: Joystick, grey thumb hat up
-- d0_b15: Joystick, grey thumb hat right
-- d0_b16: Joystick, grey thumb hat down
-- d0_b17: Joystick, grey thumb hat left
-- d0_b18: Joystick, grey thumb hat press
-- ---------------------------------------
-- d1_a0: Throttle, stupid nipple vertical
-- d1_a1: Throttle, stupid nipple horizontal
-- d1_a2: Throttle, right side
-- d1_a3: Throttle, left side
-- d1_a4: Throttle, grey rocker
-- d1_a5: Throttle, index finger hat horizontal
-- d1_a6: Throttle, index finger hat vertical
-- d1_b0: Throttle, stupid nipple press
-- d1_b1: Throttle, thumb hat press
-- d1_b2: Throttle, thumb hat up
-- d1_b3: Throttle, thumb hat forward
-- d1_b4: Throttle, thumb hat down
-- d1_b5: Throttle, thumb hat back
-- d1_b6: Throttle, top grey rocker forward
-- d1_b7: Throttle, top grey rocker back
-- d1_b8: Throttle, bottom grey rocker forward
-- d1_b9: Throttle, bottom grey rocker back
-- d1_b10: Throttle, red rocker foward
-- d1_b11: Throttle, red rocker back
-- d1_b12: Throttle, switch on left of throttle, forward
-- d1_b13: Throttle, switch on left of throttle, back
-- d1_b14: Throttle, pinky red button
-- d1_b15: Throttle, ENG L
-- d1_b16: Throttle, FLOW R
-- d1_b17: Throttle, ENG OPER L NORM
-- d1_b18: Throttle, ENG OPER R NORM
-- d1_b19: Throttle, APU START
-- d1_b20: Throttle, L/G WRN
-- d1_b21: Throttle, Flaps up
-- d1_b22: Throttle, Flaps down
-- d1_b23: Throttle, EAC
-- d1_b24: Throttle, RDR ALTM
-- d1_b25: Throttle, Autopilot Engage/disengage
-- d1_b26: Throttle, LASTE PATH
-- d1_b27: Throttle, LASTE ALT
-- d1_b28: Throttle, Throttle off
-- d1_b29: Throttle, Throttle off
-- d1_b30: Throttle, ENG OPER L IGN
-- d1_b31: Throttle, ENG OPER R IGN
--
-- Left/right axis
function d0_a0_event(value)
    send_axis_event(0, 0, value)
end

-- Up/down axis
function d0_a1_event(value)
    send_axis_event(0, 1, value)
end

-- Grey thumb hat x axis
-- Power to shields
function d0_a2_event(value)
    if value > 32000 then
        send_keyboard_event(KEY_F10, 1)
        send_keyboard_event(KEY_F10, 0)
    elseif value < -32000 then
        send_keyboard_event(KEY_LEFTSHIFT, 1)
        send_keyboard_event(KEY_F10, 1)
        send_keyboard_event(KEY_F10, 0)
        send_keyboard_event(KEY_LEFTSHIFT, 0)
    end
end
-- Grey thumb hat y axis
-- Power to lasers
function d0_a3_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value < -32000 then
            send_keyboard_event(KEY_F8, 1)
            send_keyboard_event(KEY_F8, 0)
        end
    else
        if value < -32000 then
            send_keyboard_event(KEY_F9, 1)
            send_keyboard_event(KEY_F9, 0)
        elseif value > 32000 then
            send_keyboard_event(KEY_LEFTSHIFT, 1)
            send_keyboard_event(KEY_F9, 1)
            send_keyboard_event(KEY_F9, 0)
            send_keyboard_event(KEY_LEFTSHIFT, 0)
        end
    end
end

-- Fire lasers
function d0_b0_event(value)
    send_button_event(0, 0, value)
end

-- Cycle weapon settings
function d0_b1_event(value)
    if value == 1 then
        send_keyboard_event(KEY_W, 1)
        send_keyboard_event(KEY_W, 0)
    end
end

-- Toggle beam weapon
function d0_b2_event(value)
    if value == 1 then
        send_keyboard_event(KEY_B, 1)
        send_keyboard_event(KEY_B, 0)
    end 
end

-- d0_b3: Joystick, pinky lever
-- Mode switch 
-- function d0_b3_event(value)
--     send_button_event(0, 3, value)
-- end

-- d0_b4: Joystick, index finger button on right of stick
-- Roll/Target current ship in sights
-- Alt: Fire countermeasures
function d0_b4_event(value)
    send_button_event(0, 1, value)
end

-- d0_b5: Joystick, main trigger fully down
-- Fire-link weapons
function d0_b5_event(value)
    if value == 1 then
        send_keyboard_event(KEY_X, 1)
        send_keyboard_event(KEY_X, 0)
    end
end

-- d0_b6: Joystick, round black hat up
-- R Target nearest enemy fighter
-- Alt: I Target nearest incoming warhead
function d0_b6_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value == 1 then
            send_keyboard_event(KEY_I, 1)
            send_keyboard_event(KEY_I, 0)
        end
    else
        if value == 1 then
            send_keyboard_event(KEY_R, 1)
            send_keyboard_event(KEY_R, 0)
        end
    end
end

-- d0_b7: Joystick, round black hat right
-- T Cycle through targets
-- Alt: U Target newest craft
function d0_b7_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value == 1 then
            send_keyboard_event(KEY_U, 1)
            send_keyboard_event(KEY_U, 0)
        end
    else
        if value == 1 then
            send_keyboard_event(KEY_T, 1)
            send_keyboard_event(KEY_T, 0)
        end
    end
end

-- d0_b8: Joystick, round black hat down
-- E Target nearest craft targeting you
-- Alt: O Target nearest objective craft
function d0_b8_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value == 1 then
            send_keyboard_event(KEY_O, 1)
            send_keyboard_event(KEY_O, 0)
        end
    else
        if value == 1 then
            send_keyboard_event(KEY_E, 1)
            send_keyboard_event(KEY_E, 0)
        end
    end
end

-- d0_b9: Joystick, round black hat left
-- Y Cycle backwards through targets
-- Alt: A Target attacker of target
function d0_b9_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value == 1 then
            send_keyboard_event(KEY_A, 1)
            send_keyboard_event(KEY_A, 0)
        end
    else
        if value == 1 then
            send_keyboard_event(KEY_Y, 1)
            send_keyboard_event(KEY_Y, 0)
        end
    end
end

-- d0_b10: Joystick, flat black hat up
-- Cycle through your target's components
-- Alt: Reverse cycle through your target's components
function d0_b10_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value == 1 then
            send_keyboard_event(KEY_LEFTSHIFT, 1)
            send_keyboard_event(KEY_COMMA, 1)
            send_keyboard_event(KEY_COMMA, 0)
            send_keyboard_event(KEY_LEFTSHIFT, 0)
        end
    else
        if value == 1 then
            send_keyboard_event(KEY_COMMA, 1)
            send_keyboard_event(KEY_COMMA, 0)
        end
    end
end

-- d0_b11: Joystick, flat black hat right
-- Select target preset #1
-- Alt: Save target in preset #1
function d0_b11_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value == 1 then
            send_keyboard_event(KEY_LEFTSHIFT, 1)
            send_keyboard_event(KEY_F5, 1)
            send_keyboard_event(KEY_F5, 0)
            send_keyboard_event(KEY_LEFTSHIFT, 0)
        end
    else
        if value == 1 then
            send_keyboard_event(KEY_F5, 1)
            send_keyboard_event(KEY_F5, 0)
        end
    end
end

-- d0_b12: Joystick, flat black hat down
-- Select target preset #2
-- Alt: Save target in preset #2
function d0_b12_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value == 1 then
            send_keyboard_event(KEY_LEFTSHIFT, 1)
            send_keyboard_event(KEY_F6, 1)
            send_keyboard_event(KEY_F6, 0)
            send_keyboard_event(KEY_LEFTSHIFT, 0)
        end
    else
        if value == 1 then
            send_keyboard_event(KEY_F6, 1)
            send_keyboard_event(KEY_F6, 0)
        end
    end
end

-- d0_b13: Joystick, flat black hat left
-- Select target preset #3
-- Alt: Save target in preset #3
function d0_b13_event(value)
    mode = get_button_status(0, 3)
    if mode == 1 then
        if value == 1 then
            send_keyboard_event(KEY_LEFTSHIFT, 1)
            send_keyboard_event(KEY_F7, 1)
            send_keyboard_event(KEY_F7, 0)
            send_keyboard_event(KEY_LEFTSHIFT, 0)
        end
    else
        if value == 1 then
            send_keyboard_event(KEY_F7, 1)
            send_keyboard_event(KEY_F7, 0)
        end
    end
end

-- Up arrow key
function d0_b14_event(value)
    if value == 1 then
        send_keyboard_event(KEY_UP, 1)
        send_keyboard_event(KEY_UP, 0)
    end
end

-- Right arrow key
function d0_b15_event(value)
    if value == 1 then
        send_keyboard_event(KEY_RIGHT, 1)
        send_keyboard_event(KEY_RIGHT, 0)
    end
end

-- Down arrow key
function d0_b16_event(value)
    if value == 1 then
        send_keyboard_event(KEY_DOWN, 1)
        send_keyboard_event(KEY_DOWN, 0)
    end
end

-- Left arrow key
function d0_b17_event(value)
    if value == 1 then
        send_keyboard_event(KEY_LEFT, 1)
        send_keyboard_event(KEY_LEFT, 0)
    end
end

-- Select item in menu
function d0_b18_event(value)
    if value == 1 then
        send_keyboard_event(KEY_ENTER, 1)
        send_keyboard_event(KEY_ENTER, 0)
    end
end


-- Throttle
function d1_a3_event(value)
    send_axis_event(0, 2, value)
end

-- Rudder
function d1_a0_event(value)
    send_axis_event(0, 0, value)
end

-- d1_a5: Throttle, index finger hat horizontal
function d1_a5_event(value)
    if value < -3200 then
        send_keyboard_event(KEY_F, 1)
        send_keyboard_event(KEY_F, 0)
    elseif value > 3200 then
        send_keyboard_event(KEY_G, 1)
        send_keyboard_event(KEY_G, 0)
    end
end

-- d1_b0: Stupid nipple press
-- Cycle shield settings
function d1_b0_event(value)
    if value == 1 then
        send_keyboard_event(KEY_S, 1)
        send_keyboard_event(KEY_S, 0)
    end
end

-- d1_b1: Throttle, thumb hat press
-- Attack my target
-- Alt: Ignore my target
function d1_b1_event(value)
    mode = get_button_status(0, 3)
    if value == 1 then
        send_keyboard_event(KEY_LEFTSHIFT, 1)
        if mode == 1 then
            send_keyboard_event(KEY_I, 1)
            send_keyboard_event(KEY_I, 0)
        else
            send_keyboard_event(KEY_A, 1)
            send_keyboard_event(KEY_A, 0)
        end
        send_keyboard_event(KEY_LEFTSHIFT, 0)
    end
end
-- d1_b2: Throttle, thumb hat up
-- Cover me
-- Alt: Evade
function d1_b2_event(value)
    mode = get_button_status(0, 3)
    if value == 1 then
        send_keyboard_event(KEY_LEFTSHIFT, 1)
        if mode == 1 then
            send_keyboard_event(KEY_E, 1)
            send_keyboard_event(KEY_E, 0)
        else
            send_keyboard_event(KEY_C, 1)
            send_keyboard_event(KEY_C, 0)
        end
        send_keyboard_event(KEY_LEFTSHIFT, 0)
    end
end
-- d1_b3: Throttle, thumb hat forward
-- Go ahead
-- Alt: Wingmate commands MFD
function d1_b3_event(value)
    mode = get_button_status(0, 3)
    if value == 1 then
        if mode == 1 then
            send_keyboard_event(KEY_TAB, 1)
            send_keyboard_event(KEY_TAB, 0)
        else
            send_keyboard_event(KEY_LEFTSHIFT, 1)
            send_keyboard_event(KEY_G, 1)
            send_keyboard_event(KEY_G, 0)
            send_keyboard_event(KEY_LEFTSHIFT, 0)
        end
    end
end
-- d1_b4: Throttle, thumb hat down
-- Board to reload
-- Alt: Head home
function d1_b4_event(value)
    mode = get_button_status(0, 3)
    if value == 1 then
        send_keyboard_event(KEY_LEFTSHIFT, 1)
        if mode == 1 then
            send_keyboard_event(KEY_H, 1)
            send_keyboard_event(KEY_H, 0)
        else
            send_keyboard_event(KEY_B, 1)
            send_keyboard_event(KEY_B, 0)
        end
        send_keyboard_event(KEY_LEFTSHIFT, 0)
    end
end
-- d1_b5: Throttle, thumb hat back
-- Wait for orders
-- Alt: Report in
function d1_b5_event(value)
    mode = get_button_status(0, 3)
    if value == 1 then
        send_keyboard_event(KEY_LEFTSHIFT, 1)
        if mode == 1 then
            send_keyboard_event(KEY_R, 1)
            send_keyboard_event(KEY_R, 0)
        else
            send_keyboard_event(KEY_W, 1)
            send_keyboard_event(KEY_W, 0)
        end
        send_keyboard_event(KEY_LEFTSHIFT, 0)
    end
end

-- d1_b8: Throttle, bottom grey rocker forward
-- M: Toggle map
function d1_b8_event(value)
    send_keyboard_event(KEY_M, 1)
    send_keyboard_event(KEY_M, 0)
end

-- d1_b10: Throttle, red rocker foward
-- Target nearest nav point
function d1_b10_event(value)
    if value == 1 then
        send_keyboard_event(KEY_N, 1)
        send_keyboard_event(KEY_N, 0)
    end
end

-- d1_b11: Throttle, red rocker back
-- Dock with target
function d1_b11_event(value)
    if value == 1 then
        send_keyboard_event(KEY_LEFTSHIFT, 1)
        send_keyboard_event(KEY_D, 1)
        send_keyboard_event(KEY_D, 0)
        send_keyboard_event(KEY_LEFTSHIFT, 0)
    end
end

-- d1_b14: Throttle, pinky red button
-- Fire countermeasures
-- Alt: Eject
function d1_b14_event(value)
    mode = get_button_status(0, 3)
    if value == 1 then
        if mode == 1 then
            send_keyboard_event(KEY_LEFTALT, 1)
            send_keyboard_event(KEY_E, 1)
            send_keyboard_event(KEY_E, 0)
            send_keyboard_event(KEY_LEFTALT, 0)
        else
            send_keyboard_event(KEY_C, 1)
            send_keyboard_event(KEY_C, 0)
        end
    end
end

-- d1_b16: Throttle, FLOW R
-- Toggle S-Foil
function d1_b16_event(value)
    if value == 1 then
        send_keyboard_event(KEY_V, 1)
        send_keyboard_event(KEY_V, 0)
    end
end

-- d1_b18: Throttle, ENG OPER R NORM
-- Padlock: switches your view from reticule to targeted object
function d1_b18_event(value)
    if value == 1 then
        send_keyboard_event(KEY_L, 1)
        send_keyboard_event(KEY_L, 0)
    end
end

-- d1_b23: Throttle, EAC
-- Pickup objects, or release them
function d1_b23_event(value)
    send_keyboard_event(KEY_LEFTSHIFT, 1)
    if value == 1 then
        send_keyboard_event(KEY_P, 1)
        send_keyboard_event(KEY_P, 0)
    else
        send_keyboard_event(KEY_R, 1)
        send_keyboard_event(KEY_R, 0)
    end
    send_keyboard_event(KEY_LEFTSHIFT, 0)
end

-- d1_b25: Throttle, Autopilot Engage/disengage
-- Enter hyperspace (press space)
function d1_b25_event(value)
    if value == 1 then
        send_keyboard_event(KEY_SPACE, 1)
        send_keyboard_event(KEY_SPACE, 0)
    end
end

-- d1_b31: Throttle, ENG OPER R IGN
-- Toggle cockpit
function d1_b31_event(value)
    if value == 1 then
        send_keyboard_event(KEY_DOT, 1)
        send_keyboard_event(KEY_DOT, 0)
    end
end
