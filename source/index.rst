.. Input Maps documentation master file, created by
   sphinx-quickstart on Wed Mar 29 20:47:46 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Input Maps
==========

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   general_info
   elite_dangerous
   star_wars_squadrons
   war_thunder
   xwing_alliance
   template


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
