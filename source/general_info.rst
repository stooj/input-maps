General Info
============

Molten Gamepad
--------------

- `GitHub - jgeumlek/MoltenGamepad: Flexible Linux input device translator, geared for gamepads <https://github.com/jgeumlek/MoltenGamepad>`_
- `MoltenGamepad/permissions_(udev).md at master · jgeumlek/MoltenGamepad · GitHub <https://github.com/jgeumlek/MoltenGamepad/blob/master/documentation/permissions_(udev).md>`_
