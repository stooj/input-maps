Input Maps
==========

I have an Atreus Keyboard and a Thrustmaster HOTAS Warthog.

This repo contains configurations for those peripherals.

Keyboard
--------

Keyboard layouts are stored in a json file. They can be compiled locally using a
QMK tool, or online at the `QMK configurator <https://config.qmk.fm/#/atreus/LAYOUT>`_

Joystick
--------

My joystick mappings use a brilliant tool called wejoy_.

.. _wejoy: https://github.com/Vantskruv/wejoy

Usage
"""""

1. Ensure the ``uinput`` module is loaded:

   .. code-block:: bash
   
      sudo modprobe uinput

2. Run the desired map file with wejoy:

   .. code-block:: bash

      sudo wejoy /path/to/config.lua

Why is there a blank configuration?
"""""""""""""""""""""""""""""""""""

X-Wing Alliance (and possibly others) will **only** use ``/dev/input/js0``. To
make sure that the wejoy configuration is the first device, I use the following
method:

1. Unplug joysticks; there should be no ``/dev/input/js*`` devices.
2. Run the ``blank.lua`` configuration; this will reserve ``/dev/input/js0``
3. Plug in the stick (``/dev/input/js1``), then the throttle
   (``/dev/input/js2``)
4. Stop the wejoy ``blank.lua`` config. ``/dev/input/js0`` will be removed.
5. Run the wejoy configuration that you want to use.

The blank configuration uses my mouse, so it would need to be adjusted to
something suitable for your needs.

What is ``joystick_info``?
""""""""""""""""""""""""""

It's a script lifted directly from the `pygame joystick`_ page to make it easy
to identify the names of buttons and axes on the HOTAS.

.. _pygame joystick: https://www.pygame.org/docs/ref/joystick.html
